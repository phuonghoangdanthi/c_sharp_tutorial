﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingSkill.MemberVariables
{
    internal class Product
    {
        public readonly string Name = "Product Name";
        public readonly double UnitPrice = 100;
        public readonly double Discount = 5;
        public int Quantity;
        
        public Product()
        {

        }
        public Product(string name, double unitPrice, double discount, int quantity)
        {
            Name = name;
            UnitPrice = unitPrice;
            Discount = discount;
            Quantity = quantity;
        }
    }
}
