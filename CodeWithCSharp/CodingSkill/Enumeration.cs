﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingSkill
{
    public class Enumeration
    {
        private static bool value;

        /// <summary>
        /// Enum chua danh sach gioi tinh 
        /// </summary>
        enum Gender
        {
            Male, Female, Unknown
        }
        /// <summary>
        /// Enum chua danh sach cac ngay trong tuan
        /// </summary>
        enum DayOfWeek
        {
            Monday = 2,
            Tuesday = 3,
            Wednesday = 4,
            Thursday = 5,
            Friday = 6,
            Saturday = 7,
            Sunday = 8,
        }
        public enum Color : byte
        {
            Red = 1,
            Green = 2,
            Blue = 3,
        }
        public enum Month
        {
            Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec
        }

        public static void PrintEnum()
        {
            var gender = Gender.Male;
            Console.WriteLine($"My gender is {gender} ({(int)gender})");
            var day = DayOfWeek.Tuesday;
            Console.WriteLine($"Today is {day} ({(int)day})");
            var color = Color.Blue;
            Console.WriteLine($"My favorite color is {color} ({(int)color})");
            var month = Month.Aug;
            Console.WriteLine($"My birth month is {month} ({(int)month})");
            Console.WriteLine("--------------------------");
        }
        public static void ShowEnum()
        {
            var item = Enum.GetNames(typeof(Gender));
            Console.WriteLine(String.Join(" ", item));
            Console.WriteLine();
            foreach (int i in Enum.GetValues(typeof(DayOfWeek)))
            {
                Console.WriteLine("{0} = {1}", (DayOfWeek)i, i );
            }

        }
    }
}
