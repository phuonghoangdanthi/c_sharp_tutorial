﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingSkill.Loop
{
    public class DoWhile
    { 
        public static void InputNumber()
        {
            float number;
            Console.Write("Input number: ");
            number = float.Parse(Console.ReadLine());
            while (!(1 <= number && number <= 100))
            {
                Console.WriteLine("Re-input number");
                number = float.Parse(Console.ReadLine());
            }
            Console.WriteLine("After input, number is {0}", number);
        }
        
        public static void CountNumber( int count, int count1 )
        {
            Console.Write("Ascending: ");
            while (count <= 10)
            {
                Console.Write(count + " ");
                count++;
            }
            
            Console.WriteLine();
            Console.Write("Decreasing: ");
            while (count1 >= 1)
            {
                Console.Write(count1 + " ");
                count1--;
            }
            Console.ReadLine();

        }


    }
}
