﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using OOP_Property.PropertyWithBackedField;
//using OOP_Property.AutoProperty;
using OOP_Property.Property_List;

namespace OOP_Property
{
    internal class Program
    {
        static void Main(string[] args)
        {
            /// <summary>
            /// PropertyWithBackedField
            /// </summary>

            /*           Book book = new Book();
                       // lệnh này lỗi, vì setter của Id là protected
                       // chỉ có thể gán giá trị cho Id từ trong class
                       // không thể gán giá trị từ ngoài class
                       //book.Id = 2;
                       book.Authors = "Christian Nagel";
                       book.Title = "Professional C# 7 and .NET Core";
                       book.Publisher = "Wrox";
                       book.Year = 2018;
                       book.Description = "The best book ever about the new C# 7 and the .NET Core";
                       Console.WriteLine($"{book.Id}, {book.Authors}, {book.Title}, - {book.Publisher}, {book.Year}, {book.Description}");
           */
            /// <summary>
            /// Auto-property
            /// </summary>

            /*           var book = new Book();
                       // lệnh này lỗi, vì setter của Id là protected
                       // chỉ có thể gán giá trị cho Id từ trong class
                       // không thể gán giá trị từ ngoài class
                       //book.Id = 2;
                       book.Authors = "Christian Nagel";
                       book.Title = "Professional C# 7 and .NET Core";
                       book.Publisher = "Wrox";
                       book.Year = 2018;
                       book.Description = "The best book ever about the new C# 7 and the .NET Core";
                       Console.WriteLine($"{book.Authors}, {book.Title}, - {book.Publisher}, {book.Year}");
           */

            /// <summary>
            /// Tao Property kieu List
            /// </summary>

            ExamListPro list = new ExamListPro();
            list.L1.Add(12);
            foreach (var item in list.L1)
            {
                Console.WriteLine(item);
            }

            Console.ReadKey();
        }
    }
}
