﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Property.PropertyWithBackedField
{
    internal class Book
    {
        /// <summary>
        /// Sach dien tu
        /// </summary>
        private int _id = 1;
        private string _authors = "Unknown author";
        private string _title = "A new book";
        private string _publisher = "Unknown publisher";
        private int _year = 2018;
        private string _description;
        public int Id
        {
            get { return _id; }
            protected set
            {
                _id = value;
            }
        }
        /// <summary>
        /// tên tác giả/ nhóm tác giả
        /// </summary>
        public string Authors
        {
            get { return _authors; }
            set
            {
                _authors = value;
            }
        }
        /// <summary>
        /// tiêu đề
        /// </summary>
        public string Title
        {
            get { return _title; }
            set
            {
                _title = value;
            }
        }
        /// <summary>
        /// nhà xuất bản
        /// </summary>
        public string Publisher
        {
            get { return _publisher; }
            set
            {
                _publisher = value;
            }
        }
        /// <summary>
        /// năm xuất bản
        /// </summary>
        public int Year
        {
            get { return _year; }
            set
            {
                _year = value;
            }
        }
        /// <summary>
        /// thông tin mô tả
        /// </summary>
        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
            }
        }
    }
}
