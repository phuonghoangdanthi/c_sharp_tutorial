﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Property.AutoProperty
{
    internal class Book
    {
        public int Id { get; protected set; } = 1;
        /// <summary>
        /// tên tác giả/ nhóm tác giả
        /// </summary>
        public string Authors { get; set; } = "Unknown author";
        /// <summary>
        /// tiêu đề
        /// </summary>
        public string Title { get; set; } = "A new book";
        /// <summary>
        /// nhà xuất bản
        /// </summary>
        public string Publisher { get; set; } = "Unknown publisher";
        /// <summary>
        /// năm xuất bản
        /// </summary>
        public int Year { get; set; } = 2018;
        /// <summary>
        /// thông tin mô tả
        /// </summary>
        public string Description { get; set; }
    }
}
