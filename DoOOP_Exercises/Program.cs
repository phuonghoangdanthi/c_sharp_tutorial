﻿//using DoOOP_Exercises.Inheritance;
using DoOOP_Exercises.Polymorphism;
//using DoOOPExercises.Abstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace DoOOP_Exercises
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            // Inheritance - Tinh ke thua
            /*
                        Student student = new Student();
                        Person person = new Student();
                        Person person1 = new Person();
                        // student.showInfo();
                        person.Say();
                        person1.Say();
                        Console.Read();
            */
            // student.GetScholarship();

            // Polymorphism - Tinh da hinh 
            Student student = new Student();
            //Teacher teacher = new Teacher();
            // virtual - override 
            //student.Say();
            //Console.WriteLine();
            //teacher.Say();
            // abstract - override
/*            student.Work();
            teacher.Work();*/
            //Person student1 = new Student();
            //Person teacher1 = new Teacher();
           // student1.Say();
           // teacher1.Say();      
      

            // Interface - Product
/*            HinhChuNhat h = new HinhChuNhat(4,5);
            h.TinhChuVi();
            Console.WriteLine("Chu vi Hinh chu nhat {0}", h.TinhChuVi());
            h.TinhDienTich();
            Console.WriteLine("Dien tich hinh chu nhat {0} ", h.TinhDienTich());

            Console.WriteLine();
            HinhTron hinhTron = new HinhTron();
            hinhTron.Radius = 5;
            hinhTron.TinhChuVi();
            Console.WriteLine("Chu vi hinh Tam giac {0}", hinhTron.TinhChuVi());
            hinhTron.TinhDienTich();
            Console.WriteLine("Dien tich hinh Tam giac {0}", hinhTron.TinhDienTich());*/


        }
    }
}
