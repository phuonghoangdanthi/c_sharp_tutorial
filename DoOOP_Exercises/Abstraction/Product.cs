﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoOOP_Exercises.Abstraction
{
     interface IHinhHoc
    {
         double TinhChuVi();
         double TinhDienTich();
    }
    class HinhChuNhat : IHinhHoc
    {
        public double Weigth { get; set; }
        public double Height { get; set; }
        public HinhChuNhat()
        { }
        public HinhChuNhat(double w, double h)
        {
            Weigth = w;
            Height = h;
        }
        public double TinhChuVi()
        {
            return 2 * (Weigth + Height);
        }

        public double TinhDienTich()
        {
            return Weigth * Height;

        }
    }
    class HinhTron: IHinhHoc
    {
        public double Radius {  get; set;}
        public HinhTron()
        {

        }
        public HinhTron(double radius)
        {
            Radius = radius;
        }

        public double TinhChuVi()
        {
            return 2*Radius* Math.PI;
        }

        public double TinhDienTich()
        {
            return Math.Pow(2,Radius)* Math.PI ;
        }
    }

}
