﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoOOP_Exercises.Inheritance
{
    public class Student : Person
    {
        public string Id { get; set; }
        public double Gpa { get; set; }
        public string Email { get; set; }
        public Student(): base()
        {
            // Tao ham (Contructor) dua vao ham o Class Person
        }
       
        public Student(string name, string gender, DateTime dob, string address) : base(name, gender, dob, address)
        {
            // Tao ham (contructor) co tham so dua vao ham o Class Person
            
        }

        public Student(string id, double gpa, string email)
            {
              
            }
            ~Student()
        {

        }
        public new void inputInfo()
        {
            base.inputInfo();
            Console.Write("Nhập Mã số sinh viên: ");
            Id = Console.ReadLine();
            Console.Write("Nhập Điểm trung bình:");
            Gpa = double.Parse(Console.ReadLine());
            Console.Write("Nhập Email: ");
            Email = Console.ReadLine();
        }
        public new void showInfo() // tao mot ham info 
        {
            base.showInfo();// xuat nhung thong tin da tao o Class Person
            Console.WriteLine("Mã số sinhh viên: "+Id);
            Console.WriteLine("Điểm trung bình: "+Gpa);
            Console.WriteLine("Email: "+Email);
        }
            public void GetScholarship()
        {
            if (Gpa >=8)
            {
                Console.WriteLine("Sinh viên được nhận học bổng");
            }    
            else
            {
                Console.WriteLine("Sinh viên không nhận được học bổng");
            }  
        }
        public void Learn()
        {
            base.showInfo();
        }
        public override void Say()
        {
            Console.WriteLine("child");
        }

    }

    public interface IStudent
    {
        void Learn();
    }
}
