﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoOOP_Exercises.Inheritance
{
    public class StudentTest
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding= Encoding.UTF8;      
            Console.Write("Nhập số lượng sinh viên: ");
            int n=int.Parse(Console.ReadLine());
            for (int i = 1; i <= n; i++)
            {
                Console.WriteLine("Sinh viên {0}",i);
                Student student = new Student();
                student.inputInfo();
                student.showInfo();
            }
            Console.ReadLine();
        }
    }
}
