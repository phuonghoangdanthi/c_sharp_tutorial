﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoOOP_Exercises.Inheritance
{
    public class Person
    {
        private string Name { get; set; }
        private string Gender { get; set; }
        private DateTime DayOfBirth { get; set; }
        private string Address { get; set; }
        public Person()
        {

        }
        public Person(string name, string gender, DateTime dob, string address)
        {
            Name = name;
            Gender = gender;
            DayOfBirth = dob;
            Address = address;
        }
        ~Person()
        {

        }
        public void inputInfo()
        {
            Console.WriteLine("Nhập thông tin Person");
            Console.Write("Nhập Họ và tên: ");
            Name = Console.ReadLine();
            Console.Write("Nhập Giới tính: ");
            Gender = Console.ReadLine();
            Console.Write("Nhập Ngày sinh: ");
            DayOfBirth = DateTime.Parse(Console.ReadLine());
            Console.Write("Nhập Địa chỉ: ");
            Address = Console.ReadLine();
        }
        public void showInfo()
        {
            Console.WriteLine("Thông tin của Person:");
            Console.WriteLine("Họ và tên: " + Name);
            Console.WriteLine("Giới tính: " + Gender);
            Console.WriteLine("Ngày sinh: " + DayOfBirth);
            Console.WriteLine("Địa chỉ: " + Address);
        }
        public virtual void Say()
        {
            Console.WriteLine("parent");
        }
    }
   
}
// https://v1study.com/csharp-bai-tap-phan-thua-ke-va-da-hinh.html