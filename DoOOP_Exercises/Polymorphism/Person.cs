﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoOOP_Exercises.Polymorphism
{
    internal class Person
    {
        public String Name { get; set; }
        public String Gender { get; set; }
        public Person()
        {

        }
        public Person(string name, string gender)
        {
            Name = name;
            Gender = gender;
        }
        public virtual void Say() // virtual - Phuong thuc ao Say()
        {
            Console.WriteLine("Person say something....");
        }

       
        //public abstract void Work();
/*        { 
            Console.WriteLine("Hard Work");
        }*/
    }

}
