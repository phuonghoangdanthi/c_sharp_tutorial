﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoOOP_Exercises.Polymorphism
{
    internal class Teacher:Person
    {
        public Teacher() : base()
        {

        }
        public Teacher(string name, string gender) : base (name, gender)
        {

        }
        public override void Say()
        {
            Console.WriteLine("Teacher say something........");
            base.Say();
        }
/*        public override void Work()
        {
            Console.WriteLine("Teacher is Teaching ");
        }*/
    }
}
