﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoOOP_Exercises.Polymorphism
{
    internal class Student : Person
    {
        public Student() : base()
        {

        }
        public Student(string name, string gender) : base(name, gender)
        {

        }
        // override - nap chong phuong thuc
        public override void Say()

        {
            Console.WriteLine("Student say something........");
            base.Say();
        }
        // public override void Work() => Console.WriteLine("Student is Learning");
        /*        {
                    Console.WriteLine("Student is Learning");
                }*/

        public double Math { get; set; }
        public double Chemistry { get; set; }
        public double CheckScore(double math, double chemistry)
        {
            Math = math;
            Chemistry = chemistry;
            return math + chemistry;
        }
        public void CheckInfo()
        {
            return;
        }
        public void ShowScore(double phys, double bio)
        {
        }

    }
}

  
