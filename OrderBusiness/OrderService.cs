﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderBusiness
{
    internal class OrderService
    {
        static void Main(string[] args)
        {

            //1. create product
            Product product = new Product(111, "Computer", "Dell", 12000000, 10);
           // product.ShowProduct();
            Product product1 = new Product(102, "Laptop", "Apple", 50000000, 10);
           // product1.ShowProduct();

            //2. create customer
            Customer customer = new Customer();
            customer.ID = 112;
            customer.Name = "Phuong";
            customer.PhoneNumber = "03454548215";
            customer.Address = "Dak Lak";

            //3. create order
            // 3.1 add customer
            
            Order order = new Order();
            order.AddCustomer(customer);
            order.ID = 111;
            order.OrderName = "Order buy Computer";
            order.NumberOfProduct = 2;

            //4. add product
            order.AddProduct(product);// add new product
            order.AddProduct(product1);
            order.ShowOrder();

            //5. cancel order
            //order.CancelOrder();
            //  order.ShowOrder();
            //6. remove product
            // order.RemoveProduct(product1);
            //order.ShowOrder();
            //7. ChangeQuatityProduct
            product1.Quantity = 1;
            order.ChangeQuantityProduct(product1);
            order.ShowOrder();

            Console.Read();

        }
    }
}
