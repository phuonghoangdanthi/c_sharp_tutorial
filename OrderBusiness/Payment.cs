﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderBusiness
{
    internal class Payment : EntityBase
    {
        public string PaymentMethod { get; set; }
        public string PaymentStatus { get; set; }
        public Payment()
        {

        }
        public Payment(string paymentMethod, string paymentStatus)
        {
            PaymentMethod = paymentMethod;
            PaymentStatus = paymentStatus;            
        }
        ~Payment()
        {

        }

    }
}
