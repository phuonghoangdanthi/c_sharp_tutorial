﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderBusiness
{
    internal class Customer: EntityBase
    {
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public Customer()
        {

        }
        public Customer(string name, string phoneNumber, string address)
        {
            Name = name;
            PhoneNumber = phoneNumber;
            Address = address;
        }
/*        public void InputInfo()
        {
            Console.Write("Input your name: ");
            Name = Console.ReadLine();
            Console.WriteLine("Input your phonenumber: ");
            PhoneNumber = int.Parse(Console.ReadLine());
            Console.WriteLine("Input your address: ");
            Address = Console.ReadLine();
        }*/
        public void ShowInfo()
        {
            Console.WriteLine("Information");
            Console.WriteLine();
            Console.WriteLine("Customer's Name: {0}", Name);
            Console.WriteLine("Customer's Phonenumber: {0}",PhoneNumber);
            Console.WriteLine("Custome's Address: {0}", Address);


        }
    }
}
