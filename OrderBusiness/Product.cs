﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderBusiness
{
    internal class Product: EntityBase
    {
        public string ProductName {  get; set; }
        public string ProductDesc { get; set; }
        public double Price { get; set; }
        public int Quantity { get; set; }
        public Product()
        {

        }
        public Product(int Id, string productName, string productDesc, double price, int quantity)
        {
            ID = Id;
            ProductName = productName;
            ProductDesc = productDesc;
            Price = price;
            Quantity = quantity;
        }
        public void ShowProduct()
        {
            Console.WriteLine("--- ProductID: {0}, ProductName {1}, Desc: {2}, Price: {3}, Quantities: {4}",  ID, ProductName, ProductDesc, Price, Quantity);
        }
    }
}
