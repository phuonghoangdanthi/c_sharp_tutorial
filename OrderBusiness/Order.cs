﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderBusiness
{
    internal class Order: EntityBase
    {
        public string OrderName {  get; set; }
        private Customer Customer { get; set; }
        private List<Product> ListProduct { get; set; } = new List<Product>();
        public int NumberOfProduct { get; set; }
        public string OrderStatus { get; set; } = "create new order";
        public Order()
        {

        }
/*        public Order(string orderID, string orderName, List<Product> listProduct, int numberOfProduct, string oderStatus)
        {
            OrderID = orderID;
            OrderName = orderName;
            ListProduct = listProduct;
            NumberOfProduct = numberOfProduct;
            OrderStatus = oderStatus;
        }*/
        public double TotalPrice()
        {
            double totalPrice = 0;
            foreach (var product in ListProduct)
            {
                // totalPrice = TotalPrice + product.Price * product.Quantity;
                totalPrice += product.Price * product.Quantity;
            }
            return totalPrice;
        }
        public void AddProduct(Product product)
        { 
            ListProduct.Add(product);

        }
        public void AddCustomer(Customer customer)
        {
            Customer = customer;
        }
        public void RemoveProduct(Product product)
        {
            if (ListProduct.Contains(product))
            {
                ListProduct.Remove(product);
            }
            Console.WriteLine("-------------- you just remove product: {0}--------------", product.ProductName);
        }
        public void CancelOrder()
        {
            OrderStatus = "Cancel Order";
        }
        public void ChangeQuantityProduct(Product product)
        {
            foreach( var item in ListProduct)
            {
                if(item.ID == product.ID)
                {
                    item.Quantity = product.Quantity;
                    break;
                }
            }
        }
        public void CompletedOrder()
        {
            OrderStatus = "Completed";
        }
        public void ShowOrder()
        {
            Console.WriteLine("--------- Information of Order -----------");
            Console.WriteLine("ID: {0}, Name: {1} ", ID, OrderName);
            foreach(var product in ListProduct)
            {
                product.ShowProduct();
            }
            Console.WriteLine("Total Price: " + TotalPrice());
            Console.WriteLine("Order Status: " + OrderStatus);
        }
    }
}
