﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Diagnostics;

namespace DoHomeWork_1.Lesson_3
{
    public class StudentEx
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string GetInfo()
        {
            return firstName + " " + lastName;
        }
    }
    public class Car
    {
        public string model;
        public string color;
        public int year;
        public void fullThrottle()
        {
            Console.WriteLine("The car is going as fast as it can!");
        }
    }

    public class Animal
    {
        public int age;
        public string name;
        public int weihgt;
        
    }
}