﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoHomeWork_1.Lesson_3
{
    public class Tree
    {
        public bool Leaf { get; set; }
        public int Age { get; set; }

    }
    public class Rose:Tree
    {
        public string Color { get; set; }
    }
}
