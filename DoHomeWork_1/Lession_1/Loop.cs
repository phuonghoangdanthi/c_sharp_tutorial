﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoHomeWork_1
{
    public class Loop
    {
        /*knows: while, do while, for       */

        //Bài 1: ok
        public static void DisplayNumber()
        {
            for (int i = 1; i <= 10; i++)
            { Console.Write(" " + i); }
        }
        //Bài 2 + 3: ok
        public static void SumNumber()
        {
            int sum = 0;
            for (int i = 1; i <= 11; i++)
            {
                sum += i;
                Console.Write("  " + i);
            }
            Console.WriteLine("The Sum is: " + sum);
        }
        // Bài 4: ok
        public static void ComputeNumber()
        {
            int a;
            int sum = 0; float average = 0;
            for (int i = 1; i <= 10; i++)
            {
                Console.Write("Number {0} ", i);
                a = int.Parse(Console.ReadLine());
                sum += a;
            }
            average = sum / 10;
            Console.WriteLine("The Sum is: " + sum);
            Console.WriteLine("The Average is: " + average);
        }
        // Bài 5+6 ok
        public static void DisplayTheCube()
        {
            Console.Write(" Input number :"); int n = int.Parse(Console.ReadLine());
            for (int i = 1; i <= n; i++)
            {
                //5.
                // Console.WriteLine("Number is {0} and cube of the {1} is {2}", i,i, Math.Pow(i,3));

                //6.
                Console.WriteLine("{0} x {1} = {2}", n, i, n * i);
            }
        }
        //Bài 7: fail
        public static void DisplayMultiplicationTable()
        {
            Console.Write("Input number"); int n = int.Parse(Console.ReadLine());
            for (int i = 1; i <= 10; i++)
            {
                for (int j = 1; j <= n; j++)
                {
                    Console.Write("{0} x {1} = {2}", j, i, i * j);
                    if (j < n)
                    {
                        Console.Write(", ");
                    }
                }
                Console.WriteLine();
            }
        } 
        // Bài 8: 
        public static void CalculateSumOddNumber()
        {
            Console.Write("Input number of terms: ");
            int n = int.Parse(Console.ReadLine());
            int sumOddNumber = 0;
            for (int i = 1; i <= 2 * n; i++)
            {
                if (i % 2 != 0)
                {
                    Console.WriteLine(i);
                    sumOddNumber += i;
                }
            }
            Console.WriteLine("Tổng =" + sumOddNumber);
        }
        // Bài 9:
        public static void DisplayThePattern()
        {
            Console.Write("Input number"); int n = int.Parse(Console.ReadLine());
            for (int i = 0; i <= n; i++)
            {
                for (int j = 0; j <= i; j++)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }
        }
        // Bài 10+11:
        public static void ShowTriangleNumber()
        {
            Console.Write("Input number");
            int n = int.Parse(Console.ReadLine());
            for (int i = 1; i <= n; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    // 10. Console.Write(j);
                    // 11. Console.Write(i);
                }
                Console.WriteLine();
            }
        }
        // Bài 12:
        public static void ShowIncreasedNumber()
        {
            int n = 1;
            for (int i = 0; i <= 9; i++)
            {
                for (int j = 0; j <= i; j++)
                {
                    Console.Write(" " + n++);
                }
                Console.WriteLine();
            }
        }
        // Bài 13
        public static void DisplayPyramidNumber()
        {
            int spc, t = 1;
            Console.Write("input number of rows : ");
            int rows = int.Parse(Console.ReadLine());
            spc = rows - 1;
            for (int i = 1; i <= rows; i++)
            {
                for (int k = spc; k >= 1; k--)
                {
                    Console.Write(" ");
                }
                for (int j = 1; j <= i; j++)
                    Console.Write("{0} ", t++);
                Console.WriteLine();
                spc--;
            }
        }
        //Bài 14:
        public static void DisplayPyramidAsterisk()
        {
            Console.Write("Input number:"); int n =int.Parse(Console.ReadLine());
            int spc = 1;
            spc = n;
            for (int i = 0;i <= n; i++)
            {
                for (int k=spc;k>=1;k--)
                {
                    Console.Write("{0}",i);
                }
                for (int j = 0;j <= i;j++)
                {
                    Console.Write("* ");
                }
                Console.WriteLine();
                spc--;
            }
        }
        //Bài 15:
        public static void CalculateTheFactorial()
        {
            Console.Write("Input number ");
            int n = int.Parse(Console.ReadLine());
            int fac = 1;
            for (int i = 1;i <= n;i++)
            {
                fac = fac * i;
            }
            Console.WriteLine("The Factorial of {0} is: {1}",n,fac);
        }
        //Bài 16:
        public static void ComputeSumOfEvenNumbers()
        {
            Console.Write("Input number "); 
            int n =int.Parse(Console.ReadLine());
            int sumEvenNumbers = 0;
            int evenNumber =1;
            Console.Write("The even numbers are : ");
            for (int i = 1; i <=n;i++)
            {
                evenNumber = 2 * i;
                sumEvenNumbers = sumEvenNumbers + evenNumber;
                Console.Write(" "+evenNumber);
            }
            Console.WriteLine();
            Console.WriteLine("The Sum of even Natural Number upto {0} terms :{1}", n, sumEvenNumbers);
        }
        //Bài 17:
        public static void ShowPyramidTheSameNumber()
        {
            Console.Write("Input number:"); int n = int.Parse(Console.ReadLine());
            int spc = 1;
            spc = n;
            for (int i = 0; i <= n; i++)
            {
                for (int k = spc; k >= 1; k--)
                {
                    Console.Write(" ");
                }
                for (int j = 1; j <= i; j++)
                {
                    Console.Write("{0} ",i);
                }
                Console.WriteLine();
                spc--;
            }
        }
        //Bài 18:
        public static void CalculateSumOfSeries()
        {
            Console.Write("Input the Value of x: "); int x= int.Parse(Console.ReadLine());
            Console.Write("Input the number of terms: "); int n =int.Parse(Console.ReadLine());
            double sumOfSeriesNumber = 1;
            int k =1  ;
            double d;
            for (int i = 1;i <= n;i++)
            {
              k = k * 2 * i * (2 * i - 1);
              d = Math.Pow(x,2*i) ;
              sumOfSeriesNumber = sumOfSeriesNumber + (Math.Pow(-1, i) * d / k);
               // Console.WriteLine("i = {0} có k= {1} và d= {2} và {3}",i,k,d,Math.Pow(-1,i));
            }
            Console.WriteLine("{0}",sumOfSeriesNumber);
            Console.WriteLine("Number of terms = "+ n);
            Console.WriteLine("value of x = "+ x);
        }
        //Bài 19:
        public static void CalculateSumOfHarmonicSeries()
        {
            Console.Write("Input number ");
            int n = int.Parse(Console.ReadLine());
            float sumSeries =0;
            for (int i = 1; i <= n; i++)
            {
                Console.Write("1/{0} + ", i);
                sumSeries = sumSeries + 1 /(float)i;
                    }
            Console.WriteLine();
            Console.WriteLine("Sum of Series upto {0} terms = {1}",n,sumSeries);
        }
        //Bài 20:
        public static void DisplayPyramidAnAsterisks()
        {
            Console.WriteLine("Input number");
            int n =int.Parse(Console.ReadLine());
            int spc = n;
            for (int i = 1;i <= n;i++)
            {
                for (int k=0;k <= spc;k++)
                { Console.Write("_"); }

                for (int j = 1;j<2*i; j++)
                { Console.Write("*"); }
                Console.WriteLine();
                spc--;
            }
        }
        //Bài 21:
        public static void CalculateSumOfTheSeries()
        {
            Console.Write("Input number ");
            int n = int.Parse(Console.ReadLine());

            double k, sumOfTheSeries = 0;
            for (int i = 1;i<=n;i++)
            {
                k = Math.Pow(10,i)- 1;
                Console.Write("{0} ",k);
                sumOfTheSeries = sumOfTheSeries + k;
            }
            Console.WriteLine();
            Console.WriteLine("The sum of the series = "+ sumOfTheSeries);
        }
        // Bài 22:
        public static void PrintTheFloysTriangle()
        {
            Console.Write("Input Number ");
            int n = int.Parse(Console.ReadLine());
            for (int i =1; i<=n;i++)
            {
                for (int j = i; (j <= 2*i-1); j++)
                {
                    if (j % 2 == 0)
                    {
                        Console.Write("0");
                    }
                    else
                    {
                        Console.Write("1");
                    }
                }    
                Console.WriteLine();
            }
        }
        // Bài 23:
        public static void CalculateTheSumOfTheSeries1()
        {
            Console.Write("Input the value of x:");
            int x = int.Parse(Console.ReadLine());
            Console.Write("Number of terms = ");
            int n = int.Parse(Console.ReadLine());
            double sumOfTheSeries = 1;
            double k = 1;
            for (int i = 1; i<n;i++)
            {
                k = k * i;
                sumOfTheSeries = sumOfTheSeries + (Math.Pow(x,i)/k);             
            }
            Console.WriteLine("The sum = "+ sumOfTheSeries);
            Console.WriteLine("Number of terms:"+n);
            Console.WriteLine("The value of x: "+x);
        }
        // Bài 24:
        public static void FindTheSumOfTheSeries()
        {
            Console.Write("Input the value of x:");
            int x=int.Parse(Console.ReadLine());
            Console.Write("Input number of terms ");
            int n =int.Parse(Console.ReadLine());
            double SumNumber = 0;
            for (int i = 1;i<=n;i++)
            {
                SumNumber =SumNumber - Math.Pow(-1,i)*Math.Pow(x,2*i-1);
            }
            Console.WriteLine("The sum = " +SumNumber);
            Console.WriteLine("Number of terms = "+n);
            Console.WriteLine("The value of x = "+x);
        }
        // Bài 25+26 :
        public static void ComputeSumTheSquareNaturalNumber()
        {
            Console.Write("Input number of terms ");
            int n = int.Parse(Console.ReadLine());
            double squareNaturalNumber;
            double theSumNumber = 0;
            double sumNumber = 0;
            double seriesNumber=0;
            Console.Write("The square natural upto 5 terms are: ");
            for (int i = 1; i<=n;i++)
            {
                squareNaturalNumber = Math.Pow(i,2);
                sumNumber = sumNumber + squareNaturalNumber;
                Console.Write(" " + squareNaturalNumber);
            }
            Console.WriteLine();
            Console.Write("The series Number: ");

            for (int i = 0;i<=n;i++)
            {
                seriesNumber = seriesNumber + Math.Pow(10, i);
                theSumNumber = theSumNumber + seriesNumber;
                Console.Write("{0} + ", seriesNumber);

            }
            Console.WriteLine();
            Console.WriteLine("The Sum of Square Natural Number = "+ sumNumber);
            Console.WriteLine("The Sum is {0}", theSumNumber);
                
        }
        // Bài 27:
        public static void CheckPerfectNumber()
        {
            Console.Write("Input the number ");
            int n = int.Parse(Console.ReadLine());
            int checkSum = 0;
            Console.Write("The positive divisor :");
            for (int i = 1; i < n; i++)
            {
                if (n % i == 0)
                {
                    Console.Write(" " + i);
                    checkSum = checkSum + i;
                }
            }
            Console.WriteLine();
            Console.WriteLine("The sum of the divisor is " + checkSum);
            if (checkSum == n)
            {
                Console.WriteLine("So, the number is perfect.");
            }
            else
            {
                Console.WriteLine("So, the number is not perfect.");
            }
        }
        // Bài 28:
        public static void FindThePerfectNumber()
        {
            Console.Write("Input the starting range or number ");
            int sNum = int.Parse(Console.ReadLine());
            Console.Write("Input the end range or number ");
            int eNum = int.Parse(Console.ReadLine());
            Console.Write("The Perfect numbers within the given range: ");
            for (int i = sNum; i < eNum; i++)
            {
                int checkSum = 0;
                for (int j = 1; j < i; j++)
                {
                    if (i % j == 0)
                    {
                        checkSum = checkSum + j;
                    }
                }
                if (checkSum == i)
                    {
                        Console.Write(i+" ");
                    }
            }           
        }
        // Bài 29:
        public static void CheckAnArmstrongNumber()
        {
            Console.Write("Input the number ");
            int n = int.Parse(Console.ReadLine());
            int checkSum =0;
            int r = 0, temp;
            Console.Write("The positive divisor :");
            for (temp = n; n!=0; n=n/10)
            {
                r = n % 10;
                checkSum = checkSum + r*r*r;

            }
            if (checkSum == temp)
            {
                Console.WriteLine("{0} is an Armstrong number.\n", temp);
            }
            else
            {
                Console.WriteLine("{0} is not an Armstrong number.\n", temp);
            }
        }
        // Bài 37:
        public static void DisplayNumberReverseOrder()
        {
            Console.Write("Input the number");
            int n = int.Parse(Console.ReadLine());
            int temp,r;
            double reverseNum = 0;
            for (int i = 1; n%10==1; i++)
            {
                for (temp=n; n!=0; n=n/10)
                {
                    r = n % 10;
                    reverseNum = reverseNum + Math.Pow(r,i);
                }
                Console.WriteLine();
                Console.WriteLine(reverseNum);
            }
 
        }

        // Bài 61:
        public static void DisplayAlphabelD()
        {
            for (int i = 1; i <= 7; i++)
            {
                for (int j = 1; j <= 5; j++)
                {
                    if (i == 1 || i == 7)
                    {
                        if (j < 5)
                            Console.Write("*");
                    }
                    else
                    {
                        if (j == 1 || j == 5)
                        {
                            Console.Write("*");
                        }
                        else
                        {
                            Console.Write(" ");
                        }
                    }
                }
                Console.WriteLine();
            }
        }
    }
    }

