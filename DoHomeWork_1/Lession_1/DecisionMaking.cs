﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoHomeWork_1
{
    public class DecisionMaking
    {
        // Bài 1:
        public static void CompareValues(int x1, int y1)
        {
            /* Console.WriteLine(" Nhap x:");
			 int x1 = int.Parse(Console.ReadLine());
			 Console.WriteLine("Nhap y");
			 int y1 = int.Parse(Console.ReadLine());*/
            if (x1 == y1)
                Console.WriteLine("{0} and {1} are equal.\n", x1, y1);
            else
                Console.WriteLine("{0} and {1} are not equal.\n", x1, y1);
        }
        // Bài 2: ok
        public static void CheckNumber(int x2)
        {
            if (x2 % 2 == 0)
                Console.WriteLine("{0} là số chẵn", x2);
            else
                Console.WriteLine("{0} là số lẻ", x2);
        }

        //Bài 3: ok
        public static void CheckNumber1(int x3)
        {
            if (x3 >= 0)
                Console.WriteLine("{0} là số dương", x3);
            else
                Console.WriteLine("{0} là số âm", x3);
        }
        // Bài 4: ok
        public static void CheckYear(int year)
        {
            if (year % 400 == 0)
                Console.WriteLine("{0} là năm nhuận", year);
            else if (year % 100 == 0)
                Console.WriteLine("{0} là năm không nhuận", year);
            else if (year % 4 == 0)
                Console.WriteLine("{0} là năm nhuận", year);
            else Console.WriteLine("{0} là năm không nhuận", year);
        }
        // Bài 5:ok
        public static void CheckAge()
        {
            Console.WriteLine("Nhập tuổi: ");
            byte age = byte.Parse(Console.ReadLine());
            if (age >= 18)
                Console.WriteLine("Xin chúc mừng! Bạn đủ điều kiện để bỏ phiếu bầu của bạn.");
            else
                Console.WriteLine("Xin lỗi ! Bạn chưa đủ điều kiện để bỏ phiếu bầu của bạn.");
            return;
        }
        // Bài 6:ok
        public static int PrintValue()
        {
            Console.WriteLine("Nhập giá trị m:");
            int m = int.Parse(Console.ReadLine());
            int n;
            if (m > 0)
                n = 1;
            else if (m == 0)
                n = 0;
            else
                n = -1;
            Console.WriteLine("Giá trị của n = " + n);
            return n;
        }
        //Bài 7: ok
        public static void CheckHight()
        {
            Console.WriteLine(" Nhập chiều cao:");
            float h = float.Parse(Console.ReadLine());
            if (h <= 150)
                Console.WriteLine("Phân loại thấp");
            else if ((h > 150) && (h <= 165))
                Console.WriteLine("Phân loại Trung bình");
            else if ((h > 165) && (h <= 195))
                Console.WriteLine("Phân loại: Cao ");
            else
                Console.WriteLine(" Phân loại: Cao bất thường");
        }
        // Bài 8: ok
        public static void FindMaxNumber()
        {
            int value1, value2, value3, maxValue;
            Console.WriteLine(" Mời nhập các giá trị value1, value 2, value 3:");
            value1 = int.Parse(Console.ReadLine());
            value2 = int.Parse(Console.ReadLine());
            value3 = int.Parse(Console.ReadLine());

            maxValue = value1;
            if (value2 > maxValue)
                maxValue = value2;
            else if (value3 > maxValue)
                maxValue = value3;

            Console.WriteLine("Max=" + maxValue);
        }
        //bài 9: ok
        public static void CheckCoordinates(float x4, float y4)
        {
            Console.WriteLine("Mời bạn nhập toạ độ x:");
            x4 = float.Parse(Console.ReadLine());
            Console.WriteLine("Mời bạn nhập toạ độ y:");
            y4 = float.Parse(Console.ReadLine());
            if (x4 > 0 && y4 > 0)
                Console.WriteLine(" Điểm tọa độ ({0},{1}) nằm trong góc phần tư thứ nhất", x4, y4);
            else if (x4 < 0 && y4 > 0)
                Console.WriteLine(" Điểm tọa độ ({0},{1}) nằm trong góc phần tư thứ tư", x4, y4);
            else if (x4 < 0 && y4 < 0)
                Console.WriteLine(" Điểm tọa độ ({0},{1}) nằm trong góc phần tư thứ ba", x4, y4);
            else if (x4 > 0 && y4 < 0)
                Console.WriteLine(" Điểm tọa độ ({0},{1}) nằm trong góc phần tư thứ hai", x4, y4);
            else
                Console.WriteLine(" Điểm tọa độ ({0},{1}) là gốc toạ độ");
            return;
        }
        // Bài 10: ok
        public static void CheckScore()
        {
            float math, physics, chemistry;
            Console.WriteLine("Nhập điểm toán:");
            math = float.Parse(Console.ReadLine());
            Console.WriteLine("Nhập điểm Lý:");
            physics = float.Parse(Console.ReadLine());
            Console.WriteLine("Nhập điểm Hoá:");
            chemistry = float.Parse(Console.ReadLine());
            #region C1
            if (math >= 65)
            {
                if (physics >= 55)
                {
                    if (chemistry >= 50)
                    {
                        if (math + physics + chemistry >= 180 || math + physics >= 140)
                        { Console.WriteLine("Thí sinh đủ điều kiện xét tuyển"); }
                        else
                            Console.WriteLine("Thí sinh không đủ điều kiện xét tuyển");
                    }
                    else
                        Console.WriteLine("Thí sinh không đủ điểm hoá ");
                }
                else
                    Console.WriteLine("Thí sinh không đủ điểm lý");
            }
            else
                Console.WriteLine("Thí sinh không đủ môn toán");
            #endregion
            #region  C2
            if (math >= 65 && physics >= 55 && chemistry >= 50 && (math + physics + chemistry >= 180 || math + physics >= 140))
            { Console.WriteLine("Thí sinh đủ điều kiện"); }
            else
            { Console.WriteLine("Thí sinh không đủ điều kiện"); }
            #endregion
            #region c3
            if (math < 65)
            {
                Console.WriteLine("Thí sinh khong đủ điều kiện mon toan");
                return;
            }

            if (physics < 55)
            {
                Console.WriteLine("Thí sinh khong đủ điều kiện mon ly");
                return;
            }

            if (chemistry < 50)
            {
                Console.WriteLine("Thí sinh khong đủ điều kiện mon hoa");
                return;
            }

            if (math + physics + chemistry >= 180 || math + physics >= 140)
            {
                Console.WriteLine("Thí sinh đủ điều kiện");

            }
            else
            {
                Console.WriteLine("Thí sinh khong đủ điều kiện");
            }
            #endregion

            return;
        }
        //Bài 11:ok
        public static void FindSolusions()
        {
            float a, b, c;
            float delta, x, x1, x2;
            Console.WriteLine("Nhập giá trị a,b,c: ");
            a = float.Parse(Console.ReadLine());
            b = float.Parse(Console.ReadLine());
            c = float.Parse(Console.ReadLine());
            if (a == 0)
            {
                if (b != 0 && c != 0)
                {
                    Console.WriteLine("Phương trình có nghiệm =" + (-c / b));
                }
                else if (c == 0 && b == 0)
                {
                    Console.WriteLine("Phương trình có vô số nghiệm");
                }
                else
                {
                    Console.WriteLine("Phương trình có vô nghiệm");
                }

                //if (b == 0 && c == 0)
                //{

                //        Console.WriteLine("Phương trình có vô số nghiệm");


                //} else if(c != 0)
                //{
                //    Console.WriteLine("Phương trình vô nghiệm");
                //}
                //else
                //{

                //}
            }
            else if (a != 0)
            {
                delta = (b * b) - (4 * a * c);
                if (delta > 0)
                {
                    x1 = (-b + (float)Math.Sqrt(delta)) / (2 * a);
                    x2 = (-b - (float)Math.Sqrt(delta)) / (2 * a);
                    Console.WriteLine("Phương trình có 2 nghiệm x1={0} và x2={1}", x1, x2);
                }
                else if (delta == 0)
                {
                    x = (-b) / (2 * a);
                    Console.WriteLine("Phương trình có nghiệm x = {0}", x);
                }
                else
                    Console.WriteLine("Phương trình xxx vô nghiệm");

            }
        }
        // Bài 12: ok
        public static void CheckInformation()
        {
            Console.WriteLine("Nhập SBD:");
            int idNumber = int.Parse(Console.ReadLine());
            Console.WriteLine("Nhập Tên:");
            string nameStudent = Console.ReadLine();
            Console.WriteLine("Nhập điểm Vật lý");
            float physics = float.Parse(Console.ReadLine());
            Console.WriteLine("Nhập điểm Hoá học");
            float chemistry = float.Parse(Console.ReadLine());
            Console.WriteLine("Nhập điểm Ứng dụng máy tính");
            float computer = float.Parse(Console.ReadLine());
            Console.WriteLine("Nhập id kiểm tra thông tin");
            int checkIdNumber = int.Parse(Console.ReadLine());
            if (checkIdNumber == idNumber)
            {
                Console.WriteLine("Số báo danh:{0}", idNumber);
                Console.WriteLine("Tên học sinh:{0}", nameStudent);
                Console.WriteLine("Điểm Vật lý:{0}", physics);
                Console.WriteLine("Tên Hoá học:{0}", chemistry);
                Console.WriteLine("Tên Ứng dụng máy tính:{0}", computer);
                Console.WriteLine("Tổng điểm:{0}", physics + chemistry + computer);
                Console.WriteLine("Trung bình môn: {0}", (physics + chemistry + computer) / 3);

            }
        }
        // Bài 13: ok
        public static void CheckTemperature(float temp)
        {
            if (temp <= 0)
            {
                Console.WriteLine("Freezing weather");
            }
            else if (temp > 0 && temp <= 10)
            {
                Console.WriteLine("Very Cold weather");
            }
            else if (temp > 10 && temp <= 20)
            {
                Console.WriteLine("Cold weather");
            }
            else if (temp > 20 && temp <= 30)
            {
                Console.WriteLine("Normal in temp");
            }
            else if (temp > 30 && temp <= 40)
            {
                Console.WriteLine("Hot");
            }
            else
            {
                Console.WriteLine("Very Hot");
            }
        }
        // Bài 14: ok
        public static void CheckTriangle()
        {
            float Edge1, Edge2, Edge3;
            Console.WriteLine("Nhập cạnh của tam giác:");
            Edge1 = float.Parse(Console.ReadLine());
            Edge2 = float.Parse(Console.ReadLine());
            Edge3 = float.Parse(Console.ReadLine());

            if (Edge1 == Edge2 && Edge1 == Edge3)
            {
                Console.WriteLine(" Equilateral: Tam giác đều");
            }
            else if (Edge1 != Edge2 && Edge1 != Edge3 && Edge2 != Edge3)
            {
                Console.WriteLine("Scalene: Tam giác thường");
            }
            else
                Console.WriteLine("Isosceles: Tam giác cân");
        }
        // Bài 15: ok
        public static void CheckAnglesOfTriangle()
        {
            Console.WriteLine("Nhập số đo của 3 góc");
            float Angle1 = float.Parse(Console.ReadLine());
            float Angle2 = float.Parse(Console.ReadLine());
            float Angle3 = float.Parse(Console.ReadLine());

            if (Angle1 + Angle2 + Angle3 == 180)
            {
                Console.WriteLine("Hinhg tam giác này hợp lệ");
            }
            else
            {
                Console.WriteLine("Hình tam giác này không hợp lệ ");
            }
            return;
        }
        //Bài 16: ok
        public static void CheckAlphabet()
        {
            Console.WriteLine("Nhập lý tự bất kỳ");
            char alphabet = char.Parse(Console.ReadLine());
            switch (alphabet)
            {
                case 'a':
                    Console.WriteLine("{0} là nguyên âm (vowel)", alphabet);
                    break;
                case 'e':
                    Console.WriteLine("{0} là nguyên âm (vowel)", alphabet);
                    break;
                case 'o':
                    Console.WriteLine("{0} là nguyên âm (vowel)", alphabet);
                    break;
                case 'u':
                    Console.WriteLine("{0} là nguyên âm (vowel)", alphabet);
                    break;
                case 'i':
                    Console.WriteLine("{0} là nguyên âm (vowel)", alphabet);
                    break;
                default:
                    Console.WriteLine("{0} là phụ âm (consonant)", alphabet);
                    break;
            }
        }
        // Bài 17: ok
        public static void CalculateProfit()
        {
            Console.WriteLine("Số tiền vốn");
            float Funds = float.Parse(Console.ReadLine());
            Console.WriteLine("Số tiền bán được ");
            float Revenue = float.Parse(Console.ReadLine());
            float profit = Revenue - Funds;
            if (profit < 0)
            {
                Console.WriteLine("Kinh doanh lỗ {0}", profit);
            }
            else if (profit == 0)
            {
                Console.WriteLine("Kinh doanh hoà vốn");
            }
            else
            {
                Console.WriteLine("Kinh doanh lãi {0}", profit);
            }
            return;
        }
        // Bài 18: ok
        public static void CalculateElectricityBill()
        {
            Console.Write("Mã khách hàng:");
            int IdCustomer = int.Parse(Console.ReadLine());
            Console.Write("Tên khách hàng:");
            string nameCustomer = Console.ReadLine();
            Console.Write("Tiêu thụ điện:");
            float unitConsumed = float.Parse(Console.ReadLine());
            float charges , amountCharges, surchageAmount, netAmount;
            if (unitConsumed >= 0 && unitConsumed <= 199)
            {
                charges = 1.2f;
                amountCharges = charges * unitConsumed;
            }
            else if (unitConsumed >= 200 && unitConsumed < 400)
            {
                charges = 1.5f;
                amountCharges = charges * unitConsumed;
            }
            else if (unitConsumed >= 400 && unitConsumed < 600)
            {
                charges = 1.8f;
                amountCharges = charges * unitConsumed;
            }
            else
            {
                charges = 2f;
                amountCharges = charges * unitConsumed;
            }

            if (amountCharges >= 400)
            {
                surchageAmount = amountCharges * 15 / 100;
            }
            else
            {
                surchageAmount = 0;
            }
            netAmount = amountCharges + surchageAmount;

            Console.WriteLine();
            Console.WriteLine("Mã khách hàng {0}", IdCustomer);
            Console.WriteLine("Tên khách hàng: " + nameCustomer);
            Console.WriteLine("Tiêu thụ số điện: " + unitConsumed);
            Console.WriteLine("Số tiền Chi phí {0} mỗi đơn vị: {1}", charges, amountCharges);
            Console.WriteLine("Số tiền phụ thu: " + surchageAmount);
            Console.WriteLine("Số tiền thực khách hàng phải thanh toán: " + netAmount);

        }
        // Bài 19: ok
        public static void DeclareEquivalent()
        {
            Console.WriteLine(" Nhập cấp");
            char grade = char.Parse(Console.ReadLine());
            switch (grade)
            {
                case 'E':
                   Console.WriteLine("You have chosen: Excellent");
                    break;
                case 'V':
                    Console.WriteLine("You have chosen: Very Good");
                    break;
                case 'G':
                    Console.WriteLine("You have chosen: Good");
                    break;
                case 'A':
                    Console.WriteLine("You have chosen: Average");
                    break;
                case 'F':
                    Console.WriteLine("You have chosen: Fail");
                    break;
            }
            return;
        }
        // Bài 24: ok
        public static void ComputeArea()
        {
            Console.WriteLine("Input your choice");
            byte choice = byte.Parse(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    Console.WriteLine("{0} is the circle ", choice);
                    Console.Write("radius of the circle ");
                    int r = int.Parse(Console.ReadLine());
                    Console.WriteLine("The area is " + (Math.PI * Math.Pow(r, 2)));
                    break;
                case 2:
                    Console.WriteLine("{0} is the rectangle", choice);
                    Console.Write("width of the rectangle ");
                    float width = float.Parse(Console.ReadLine());
                    Console.Write("height of the rectangle");
                    float height = float.Parse(Console.ReadLine());
                    Console.WriteLine("The area is " + width * height);
                    break;
                case 3:
                    Console.WriteLine("{0} is the square" + choice);
                    Console.Write("length of the rectangle");
                    float length = float.Parse(Console.ReadLine());
                    Console.WriteLine("The area is " + Math.Pow(length, 2));
                    break;
            }
        }
        // Bài 25:ok
        public static void CalculateNumber()
        {
            Console.WriteLine("Enter the first Integer :");
            int Integer1  = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter the second Integer :");
            int Integer2 = int.Parse(Console.ReadLine());
            Console.WriteLine("Input your the options :");
            int option = int.Parse(Console.ReadLine());

            switch (option)
            {
                case 1:
                    Console.WriteLine("The Addition of {0} and {1} is {2} ", Integer1, Integer2, Integer1+Integer2);
                    break;
                case 2:
                    Console.WriteLine("The Substraction of {0} and {1} is {2} ", Integer1, Integer2, Integer1 - Integer2);
                    break;
                case 3:
                    Console.WriteLine("The Multiplication of {0} and {1} is {2}" , Integer1, Integer2, Integer1 * Integer2);
                    break;
                case 4:
                    Console.WriteLine("The Division of {0} and {1} is {2} ", Integer1, Integer2, Integer1 / Integer2);
                    break;
                case 5:
                    break;
            }
        }

    }
}