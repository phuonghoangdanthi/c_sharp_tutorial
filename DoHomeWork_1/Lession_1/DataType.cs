﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoHomeWork_1
{
    public class DataType
    {
        //1. byte
        public static byte CheckData(byte a)
        {
            return a;
        }
        public static sbyte CheckDataSbyte(sbyte b)
        {
            return b;
        }
        public static string CheckDataString(string c)
        {
            return c;
        }

        public static char CheckDataChar(char d)
        {
            return d;
        }
        public static DateTime CheckDateTime(DateTime date)
        {
            return date;
        }

        //Bài 1:
        public static string PrintString(string str1, string str2, string str3)
        {
            return str3 + str2 + str1;
        }
        // Bài 2:
        public static void PrintTriangle(int num, int weight)
        {
            //vong 1: i = 6, 6 >= 1
            // J =6; 
            for (int i = 0; i <= weight; i++)
            {
                for (int j = 0; j <= weight - i; j++)
                {
                    Console.Write("{0}", num);
                }
                Console.Write("\n");
            }
        }
        // Bài 3:
        public static void CheckAccount()
        {

        }
    }
}
