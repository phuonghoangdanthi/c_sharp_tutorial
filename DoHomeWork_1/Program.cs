﻿using DoHomeWork_1.Lesson_2;
using DoHomeWork_1.Lesson_3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoHomeWork_1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;
            //byte value = DataType.CheckData(255);

            //sbyte value = DataType.CheckDataSbyte(-128);

            // string value = DataType.CheckDataString("Phuong");

            //char value = DataType.CheckDataChar('A');

            //  DateTime value = DataType.CheckDateTime(DateTime.Now);
            // Console.WriteLine("gia tri: {0}", value);
            // CalculateFatorial1();

            #region DecisionMaking

            //DecisionMaking.CompareValues(48,5); // B1: So sánh 2 số

            // DecisionMaking.CheckNumber(15); // B2: Check tính chẵn lẻ

            // DecisionMaking.CheckNumber1(45); // B3:Check số âm hay số dương

            //DecisionMaking.CheckYear(2000); //B4: Check năm này có phải năm nhuận hay không

            //DecisionMaking.CheckAge(); // B5: kiểm tra đủ tuổi đi bầu cử

            //DecisionMaking.PrintValue(); // B6: In ra giá trị n khi nhập m

            //DecisionMaking.FindMaxNumber(); //B7:Tìm giá trị Max

            // DecisionMaking.CheckHight(); //B8: Phân loại chiều cao

            // DecisionMaking.CheckCoordinates(5, 6);// B9: Kiểm tra toạ độ

            //DecisionMaking.CheckScore();

            //DecisionMaking.FindSolusions();// 

            //DecisionMaking.CheckInformation();

            //DecisionMaking.CheckTemperature(55);

            // DecisionMaking.CheckTriangle();

            // DecisionMaking.CheckAnglesOfTriangle();

            // DecisionMaking.CheckAlphabet();

            // DecisionMaking.CalculateProfit();

            // DecisionMaking.CalculateElectricityBill();

            // DecisionMaking.DeclareEquivalent();
            // DecisionMaking.ComputeArea();
            //DecisionMaking.CalculateNumber();
            #endregion

            #region DataType
            // string printString = DataType.PrintString("a", "b", "c");
            // Console.WriteLine(printString); // B1: hiển thị chữ theo thứ tự ngược lại 

            // DataType.PrintTriangle(6,7); //B2: In chuỗi 
            #endregion

            #region Loop
            // Loop.DisplayNumber();
            // Loop.SumNumber();
            // Loop.ComputeNumber();
            // Loop.DisplayTheCube();
            // Loop.DisplayMultiplicationTable();
            // Loop.CalculateSumOddNumber();
            // Loop.DisplayThePattern();
            // Loop.ShowTriangleNumber();
            // Loop.ShowIncreasedNumber();
            // Loop.DisplayPyramidNumber();
            // Loop.DisplayPyramidNumber();
            // Loop.DisplayPyramidAsterisk();
            // Loop.CalculateTheFactorial();
            // Loop.ComputeSumOfEvenNumbers();
            // Loop.ShowPyramidTheSameNumber();
            // Loop.CalculateSumOfSeries();
            // Loop.CalculateSumOfHarmonicSeries();
            // Loop.DisplayPyramidAnAsterisks();
            // Loop.CalculateSumOfTheSeries();
            // Loop.PrintTheFloysTriangle();
            // Loop.CalculateTheSumOfTheSeries1();
            // Loop.FindTheSumOfTheSeries();
            // Loop.ComputeSumTheSquareNaturalNumber();
            // Loop.CheckPerfectNumber();
            // Loop.FindThePerfectNumber();
            // Loop.CheckAnArmstrongNumber();
            // Loop.DisplayNumberReverseOrder();


            //
            #endregion

            #region Array

            // ArrayLesson.AccessArray();
            // ArrayLesson.PrintArray();
            // ArrayLesson.CountArray();
            // ArrayLesson.SortArray();
            // ArrayLesson.DisplayArray();
            // ArrayLesson.ReplaceElementInArray();
            // ArrayLesson.ReplaceElement();


            #endregion

            #region StackLesson
            // StackLesson.AccessElement();
            // StackLesson.InsertElement(0,1);
            //  Stack<int> newStack = StackLesson.CreateInstanceStack();
            //StackLesson.PopElement(newStack);


            #endregion

            #region Algorithm Search

            //int[] newArray = SearchAlgorithm.CreateArray(10);
            //int value = -100;
            //SearchAlgorithm.PrintArray(newArray);
            // int position = SearchAlgorithm.LinearSearch(newArray, value);

            //int pos = SearchAlgorithm.BinarySearch(newArray, value);
            //  Console.WriteLine("vi tri"+ pos);
            //int pos = SearchAlgorithm.InterpolationSearch(newArray, value);
            //Console.WriteLine(" gia tri {0} duoc tim thay co vi tri  {1}", value, pos);
            //SearchAlgorithm.HashTable();
            #endregion

            #region Algorithm Sort
            /*         int[] newList = AlgorithmSort.CreateArray();
                       AlgorithmSort.PrintArray(newList);
                       AlgorithmSort.BubbleSort(newList);

                       int left = 0;
                       int right = newList.Length-2;
                       AlgorithmSort.QuickSort(newList,left,right);*/


            /*            int[] input = { 6, 1, 5, 4, 22, 10, 7 };
                        int[] output = AlgorithmSort.quickSort(input,0,input.Length-1);

                        for(int i = 0; i < output.Length; i++)
                        {
                            Console.WriteLine(output[i]);
                        }*/

            // QuickSort:

            /*
                        int[] numbers = { 1, 5, 0, 34, 3, 9 };
                        Console.Write("Start array: ");
                        foreach (int number in numbers)
                        {
                            Console.Write(number + " ");
                        }
                        Console.WriteLine();
                        Console.Write("QuickSort:   ");
                        //var letters = new[] { 'f', 'g', 'a', 'm', 'o' };
                        //var names = new[] { "Peter", "Alex", "John", "Fred" };
                        //var books = new[] { new Book("4ds"), new Book("2er"), new Book("31") };

                        // Quicksort
                        AlgorithmSort.SortArray(numbers);
                        // AlgorithmSort.SortArray(letters);
                        //AlgorithmSort.SortArray(names);
                        // AlgorithmSort.SortArray(books);
                        //

                        AlgorithmSort.printArray(numbers);
                        // AlgorithmSort.PrintArray(letters);
                        // AlgorithmSort.PrintArray(names);
                        //AlgorithmSort.PrintArray(books);
            */

            #endregion


            #region OPP 
            // Class Student
            /*            Student myStudent = new Student();
                        myStudent.firstName = "Phuong";
                        myStudent.lastName = "Hoang";
                        string Information1 = myStudent.GetInfo();
                        Console.WriteLine(myStudent.firstName);
                        Console.WriteLine(Information1);
                        Student myStudent2 = new Student();
                        myStudent.firstName = "Oanh";
                        myStudent.lastName = "Nguyen";

                        string Information = myStudent.GetInfo();
                        Console.WriteLine(Information);

            */
            // Class Car
            /*
                        Car Ford = new Car();
                        Ford.model = "Mustang";
                        Ford.color = "red";
                        Ford.year = 1969;

                        Car Opel = new Car();
                        Opel.model = "Astra";
                        Opel.color = "white";
                        Opel.year = 2005;

                        Console.WriteLine(Ford.model);
                        Console.WriteLine(Opel.model);
            */

            // Class Animal
            /*            Animal Dog = new Animal();
                        Dog.age = 3;
                        Dog.name = "Milu";
                        Dog.weihgt =5;
                        Console.WriteLine(Dog.name +": "+Dog.age +" year old and "+Dog.weihgt+" kg" );
            */
            #endregion



            // Class Person
            /*            Person girl = new Person("Phuong");
                        girl.Name = "Phuong";
                        girl.GoToBedroomWith();
                        Console.WriteLine(girl.Name);

                        Person girl2 = new Person();
                        girl2.Name = "Huan";
                        Console.WriteLine("Co gai co rau: " + girl2.Name);

                        Women girl3 = new Women();
                        girl3.Name = "A";
                        girl3.Hair = true;
                        if (girl3.Hair == true)
                        {
                            Console.WriteLine("{0} la con gai",girl3.Name);
                        }
                        else
                        {
                            Console.WriteLine("{0} la con trai", girl3.Name);
                        }*/

/*          Person stu = new Person();// "Ph", "Female",DateTime.Now,"Ho Chi Minh");
            stu.inputInfo();
            Console.WriteLine();
            stu.showInfo();
            Console.WriteLine();*/


            // Class Tree 
            Tree coconut = new Tree();
            coconut.Leaf = true;
            coconut.Age = 20;
            if (coconut.Leaf==true)
            {
                Console.WriteLine("cay nay co la va do tuoi la {0}",coconut.Age);
            }
            else
                Console.WriteLine("Cay nay ko co la va do tuoi la {0}", coconut.Age);

            Console.ReadLine();

        }

    }
}
