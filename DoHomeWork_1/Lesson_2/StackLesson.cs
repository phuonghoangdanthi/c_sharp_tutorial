﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoHomeWork_1.Lesson_2
{
    public class StackLesson
    {
        // Ex1:
        public static void AccessElement(Stack<int> myStack)
        {
            Console.WriteLine("start access");
            foreach (int item in myStack)
            {
                Console.WriteLine(item);
            }
        }
        // Ex2:
        public static Stack<int> CreateInstanceStack()
        {
            Stack<int> myStack = new Stack<int>();
            myStack.Push(1);
            myStack.Push(2);
            myStack.Push(3);
            myStack.Push(4);
            return myStack;
        }
        public static void InsertElement(int newElemnent, int pos)
        {
            Stack<int> myStack = CreateInstanceStack();
            Stack<int> newStack = new Stack<int>();
            int[] arr = myStack.ToArray();
           // newStack.Push(newElemnent);
            for (int i =arr.Length-1; i>=0; i--)
            {
                if (i == pos)
                {
                    newStack.Push(newElemnent);
                }
                newStack.Push(arr[i]);
            }            
            AccessElement(newStack);
            return;
        }
        public static void PopElement(Stack<int> myStack)
        {

            for (int i = myStack.Count - 1; i >= 0; i--)
            {
                int stack =  myStack.Pop();
                Console.WriteLine("elemnt {0} count {1} ", stack, myStack.Count);

            }
            AccessElement(myStack);
        }

    }
}
