﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoHomeWork_1.Lesson_2
{
    public class ArrayLesson
    {
        public static void DeclareArray()
        {
            //1. short syntax
            int[] employees = { 1, 2, 3, 4, 5 };
            // Example: Array Declaration
            int[] arr;
            // Declare and initilization
            string[] nameEmployees = new string[] { "Phuong", "Dan", "Hoang", "Huan" };
            // use var
            var arr2 = new int[] { 2 };



        }
        //Access Array
        public static void AccessArray()
        {
            int[] employees = { 1, 2, 3, 4 };
            Console.WriteLine("gia tri dau tien" + employees[0]);
            for (int i = 0; i < employees.Length; i++)
                Console.WriteLine("Gia tri {0} la {1}", i, employees[i]);

        }

        //Bài 1:
        public static void PrintArray()
        {
            int[] arr = new int[5];
            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine("element {0} : ", i);
                arr[i] = int.Parse(Console.ReadLine());
            }
            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write("Elements in array are:" + arr[i]);
            }
        }
        // Ex1:
        public static void CountArray()
        {
            int[] arrA = { 1, 2, 3, 4, 5, 5, 4, 3, 2, 5, 1, 2 };
            int[] arrB = { 1, 2, 3, 4, 5 };
            int[] arrC = new int[arrB.Length];
            for (int i = 0; i < arrB.Length; i++)
            //foreach (int itemB in arrB)
            {
                int n = 0;
                foreach (int itemA in arrA)
                {
                    if (itemA == /*itemB)*/arrB[i])
                    {
                        n++;
                    }
                }
                arrC.Append(n);
                Console.Write(n);
                arrC[i] = n;
            }
            Console.WriteLine();
            Console.Write("{0} phần tử của mảng C là: ", arrC.Length);
            foreach (int itemC in arrC)
                Console.Write(" " + itemC);
        }
        //Ex 2:
        public static void SortArray()
        {
            int[] arrA = { 5, 1, 2, 4, 3 };
            int[] arrB = new int[arrA.Length];
            int[] temparr = arrA;
            for (int i = 0; i < arrA.Length; i++)
            {
                int min = 999;
                for (int j = 0; j < temparr.Length; j++)
                {
                    if (temparr[j] < min)
                    {
                        min = temparr[j];
                    }
                }
                temparr = temparr.Where((source) => source != min).ToArray(); // Filter mảng với các phần tử khác giá trị min ( xoá phần tử min ra khỏi temparr)
                arrB[i] = min;
            }
            Console.WriteLine();
            foreach (int itemB in arrB)
                Console.WriteLine(" " + itemB);
        }
        // ex3: duyet mang
        public static void DisplayArray(int[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine("Phan tu {0} la {1}", i, arr[i]);
            }
        }
        // Ex 4:
        public static void ReplaceElementInArray()
        {
            int[] arr = new int[] { 1, 2, 3, 4, 5 };
            for (int i = 0; i <= arr.Length; i++)
            {
                if (arr[i] == 3)
                {
                    arr[i] = 6;
                }
            }
            // call function
            DisplayArray(arr);
        }
        // ex5: thay gia tri chan thanh gia tri + 1
        public static void ReplaceElement()
        {
            int[] arr = { 1, 2, 3, 6, 8, 1, 11, 25 };
            for (int i = 0;i <arr.Length; i++)
            {
                if (arr[i]%2==0)
                {
                    arr[i] = arr[i] + 1;
                }
            }
                DisplayArray(arr);
        }
        // Ex1:
        public static void CountArray1()
        {
            int[] arrA = {1,2, 3, 4,5,5,4,3,2,5,1,2};
            int[] arrB = { 1, 2, 3, 4, 5 };
            int[] arrC = new int[arrB.Length];
            for ( int i =0; i< arrB.Length; i++ )
           //foreach (int itemB in arrB)
            {
                int n = 0;
                foreach (int itemA in arrA )
                {
                    if (itemA == /*itemB)*/arrB[i])
                    {
                        n++;
                    }               
                }
                arrC.Append(n);
                Console.Write(n);
                arrC[i] = n;
            }
            Console.WriteLine();
            Console.Write("{0} phần tử của mảng C là: ", arrC.Length);
            foreach (int itemC in arrC)
                Console.Write(" "+ itemC);
        }
        //Ex 2:
        public static void SortArray1()
        {
            int[] arrA = { 5, 1, 2, 4, 3 };
            int[] arrB = new int[arrA.Length];
            int[] temparr = arrA ;
            for (int i =0; i<arrA.Length; i++)
            {
                int min = 999;
                for (int j =0; j<temparr.Length; j++)
                {
                    if (temparr[j] < min)
                    {
                        min = temparr[j];
                    }
                }            
                temparr = temparr.Where((source)=>source!=min).ToArray(); // Filter mảng với các phần tử khác giá trị min ( xoá phần tử min ra khỏi temparr)
                arrB[i] = min;
            }
            Console.WriteLine();
            foreach (int itemB in arrB)
               Console.WriteLine(" "+itemB);

        }
    }
}
