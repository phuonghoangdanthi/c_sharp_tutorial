﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace DoHomeWork_1.Lesson_2
{
    public class AlgorithmSort
    {
        public static int[] CreateArray()
        {
            int[] newArray = { 1, 44, 5, 6, 9, 10, 25 };
            return newArray;
        }
        public static void PrintArray(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                Console.Write(array[i] + " ");
            }
            Console.WriteLine();
        }
        public static void BubbleSort(int[] newListBub)
        {
            int temp;
            int count = 0;
            for (int i = 0; i < newListBub.Length - 1; i++)
            {
                for (int j = 0; j < newListBub.Length - 1 - i; j++)
                {
                    count++;
                    if (newListBub[j] > newListBub[j + 1])
                    {
                        temp = newListBub[j];
                        newListBub[j] = newListBub[j + 1];
                        newListBub[j + 1] = temp;
                    }
                }
            }
            foreach (int item in newListBub)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine("count: {0}", count);
        }
        public static void InsertionSort(int[] listInsertion)
        {
        }
        /*        public static void QuickSort(int[] listQuick, int left, int right)
                {
                    // { 1, 44, 5, 6, 9, 10, 25 }
                    int n = listQuick.Length;
                    int pivot = listQuick[n - 1];
                    int temp;
                    int l = left;
                    int r = right;
                    while (l <= r)
                    {
                        while (listQuick[l] < pivot)
                        {
                            l++;
                        }
                        while (listQuick[r] > pivot)
                        {
                            r--;
                        }
                        if (l <= r)
                        {

                            temp = listQuick[l];
                            listQuick[l] = listQuick[r];
                            listQuick[r] = temp;
                            l++;
                            r--;
                        }
                    }
                    if (left < r)
                    {
                        QuickSort(listQuick, left, r);
                    }
                    if (right > l)
                    {
                        QuickSort(listQuick, l, right);
                    }
                    foreach (int item in listQuick)
                    {
                        Console.WriteLine("QuickSort");
                        Console.WriteLine(item + " ");
                    }
                }
                public static void swap(int[] arr, int i, int j)
                {
                    int temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }*/


        #region quickSort
        public static void Swap(int[] input, int l, int r)
        {
            int temp = input[l];
            input[l] = input[r];
            input[r] = temp;
        }
        public static int[] quickSort(int[] input, int l, int r)
        {
            if (l < r)
            {
                int pivotIndex = Partition(input, l, r);
                quickSort(input, pivotIndex, r);
                quickSort(input, pivotIndex + 1, r);
            }
            return input;
        }
        public static int Partition(int[] input, int l, int r)
        {
            int pivot = input[l];
            int pivotIndex = 1;
            l++;

            while (l <= r)
            {
                if (input[l] > pivot)
                {
                    while (l <= r)
                    {
                        if (input[r] <= pivot)
                        {
                            Swap(input, l, r);
                        }
                        r--;
                    }
                }
                l++;
            }
            Swap(input, pivotIndex, r);
            return r;
        }
        #endregion

        public static void printArray(int[] numberArray)
        {
            foreach (int element in numberArray)
            {
                  Console.Write(element+" ");
            }
            Console.WriteLine();
        }

        public static void SortArray(int[] numberArray) 
        {
            Quicksort(numberArray, 0, numberArray.Length - 1);
        }

        public static void Quicksort(int[] numberArray, int left, int right)
        {
            int indexLeft = left;
            int indexRight = right;

            var pivot = numberArray[left + (right - left) / 2];

            while (indexLeft <= indexRight)
            {
                while (numberArray[indexLeft] < pivot)
                {
                    indexLeft++;
                }


                while (numberArray[indexRight]> pivot)
                {
                    indexRight--;
                }

                if (indexLeft <= indexRight)
                {
                    var tmp = numberArray[indexLeft];
                    numberArray[indexLeft] = numberArray[indexRight];
                    numberArray[indexRight] = tmp;

                    indexLeft++;
                    indexRight--;
                }
            }

            if (left < indexRight)
                Quicksort(numberArray, left, indexRight);

            if (indexLeft < right)
                Quicksort(numberArray, indexLeft, right);
        }
    }

}




