﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace DoHomeWork_1.Lesson_2
{
    public class SearchAlgorithm
    {
        public static int[] CreateArray(int n)
        {
            int[] newArray = new int[n];

            for (int i = 0; i < n; i++)
            {
                newArray[i] = i;
            }
            return newArray;
        }
        public static void PrintArray(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                Console.Write(array[i] + " ");
            }
            Console.WriteLine();
        }
        public static int LinearSearch(int[] list, int value)
        {
            for (int i = 0; i < list.Length; i++)
            {
                if (list[i] == value)
                {
                    return i;
                }
            }
            return -1;
        }
        public static int BinarySearch(int[] list, int value)
        {
            int sta = 0;
            int end = list.Length - 1;
            int mid;
            int x = -100;
            while (true)
            {
                if (sta > end)
                {
                    return x;
                }
                mid = (sta + end) / 2;
                if (list[mid] < value)
                {
                    sta = mid;
                }
                else if (list[mid] > value)
                {
                    end = mid;
                }
                else
                {
                    return mid;
                }

                if( end - sta == 1)
                {
                    return -1;
                }
            }
        }
        public static int InterpolationSearch(int[] list, int value)
        {
            int lo=0;
            int mid1;
            int hi = list.Length-1;

            while (lo<=hi & value < list[lo] & value >list[hi] )
            {
                mid1 = lo+(value-list[lo])*(hi-lo)/(list[hi]-list[lo]);
                if(list[mid1] == value)
                {
                    return mid1;
                }
                else if (list[mid1] > value)
                {
                    hi = mid1-1;
                }
                else
                {
                    lo = mid1+1;
                }
            }
            return -1;
        }
        public static void HashTable()
        {
            Hashtable listHash = new Hashtable();
            listHash.Add(01, "Monday");
            listHash.Add(02, "Tuesday");
            listHash.Add(03, "Wednesday");
            listHash.Add(04, "Thursday");
            ICollection hashCollection = listHash.Keys;
            foreach (int key in hashCollection)
            {
                Console.WriteLine("{0}: {1}", key, listHash[key]);
            }
        }
    }
}
