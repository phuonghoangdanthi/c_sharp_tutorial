﻿-- Bai 1: Lấy danh sách tất cả các khách hàng
Select * from Customer;
-- bai2: lay ten va sdt cua tat ca cac KH
select Name,PhoneNumber from Customer;
-- b3: Lay thong tin Kh ten An
select * from Customer where Name= 'An';
-- b4: lay DS tat ca cac don hang
select * from [Order];
-- b5: Lay Order chua thong tin KH;
select * from Customer left join [Order]on Customer.ID = [Order].CustomerID;
-- b6: Lay Order chua thong tin Product
select * from [Order] inner join Product_Order on [Order].ID = Product_Order.OrderID 
inner join Product on Product_Order.ProductID =Product.ID;
-- b7: Lay thong tin Order chua thong tin KH va Product va Payment
select * from [Order] inner join Product_Order on [Order].ID = Product_Order.OrderID 
inner join Product on Product_Order.ProductID =Product.ID 
inner join Customer on Customer.ID = [Order].CustomerID
inner join Payment on Payment.ID = [Order].PaymentID;
-- b8: Lay thong tin KH co hoac khong dat hang
select * from Customer c left join [Order] o on c.ID = o.CustomerID;
-- b9: Lay Product co hoac ko dat hang
select * from Product p 
left join Product_Order po on p.ID = po.ProductID
LEFT join [Order] o on po.OrderID = o.ID; 
--B10: Dem so luong loai san pham cua moi don hang
select [Order].ID, [Order].OrderName, COUNT(Product_Order.ProductID) as NumberOfTypeProduct from [Order] inner join Product_Order on [Order].ID = Product_Order.OrderID 
inner join Product on Product_Order.ProductID =Product.ID 
inner join Customer on Customer.ID = [Order].CustomerID
inner join Payment on Payment.ID = [Order].PaymentID
group by [Order].OrderName, [Order].ID;
-- b11: Tim so luong san pham trong moi don hang
select [Order].ID, [Order].OrderName, SUM(Product_Order.Quanlity) as SumOfTypeProduct
from [Order] inner join Product_Order on [Order].ID = Product_Order.OrderID 
inner join Product on Product_Order.ProductID =Product.ID 
inner join Customer on Customer.ID = [Order].CustomerID
inner join Payment on Payment.ID = [Order].PaymentID
group by [Order].ID, [Order].OrderName;